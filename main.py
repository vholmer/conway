import random, sys, pygame as pg

from robot import *
from gui import *
from executor import *

class Main:

	def __init__(self):
		self.emptyset = set()

		self.botset = set()

		self.WIDTH = self.getInt("Enter number of rows and columns: ")

		self.HEIGHT = self.WIDTH

		self.WORLD_SIZE = self.WIDTH * self.HEIGHT

		self.MAXIMUM_SIZE = 100 ** 2

		self.checkWorldSize()

		self.NUM_DESIRED_BOTS = self.getInt("Enter desired number of bots: ")

		self.NUM_CYCLES = self.getInt("Enter desired number of steps per Space press: ")

		self.DRAWGRID = self.setupDrawGrid()

		self.SIZE_TUPLE = (self.WIDTH, self.HEIGHT)

		self.createWorld()

	def initEmptyPosList(self):
		for x in range(0, self.WIDTH):
			for y in range(0, self.HEIGHT):
				self.emptyset.add((x, y))

	def checkNumBots(self):
		if self.NUM_DESIRED_BOTS > self.WORLD_SIZE:
			print "Too many bots to fit into this world!"
			sys.exit(0)

	def createWorld(self):
		print "Please wait ... creating world."
		self.initEmptyPosList()
		self.checkNumBots()
		counter = 0
		for i in range(0, self.NUM_DESIRED_BOTS):
			self.createBot(i)
			counter += 1
			print "Bots created: " + str(counter)
		print str(len(self.botset)) + " bots successfully created."

	def createBot(self, name):
		random.seed()
		randList = random.sample(self.emptyset, 1)
		randPos = randList[0]
		self.emptyset.discard(randPos)
		self.botset.add(Robot(randPos))

	def checkWorldSize(self):
		if self.WORLD_SIZE > self.MAXIMUM_SIZE:
			print "World too large! Maximum rows/columns = " + str(int(self.MAXIMUM_SIZE ** 0.5))
			sys.exit(0)
		if self.WORLD_SIZE == 0:
			print "World size can't be zero!"
			sys.exit(0)

	def processInput(self, exe, window):
		for event in pg.event.get():
			keys = pg.key.get_pressed()
			if keys[pg.K_ESCAPE]:
				print "Program terminated."
				sys.exit(0)
			elif pg.mouse.get_pressed()[0] and event.type != pg.MOUSEMOTION:
				gW = window.getGridWidth()
				gH = window.getGridHeight()
				exe.spawn(pg.mouse.get_pos(), (gW, gH), True)
			elif keys[pg.K_SPACE] and event.type != pg.MOUSEMOTION:
				return True
			elif keys[pg.K_c] and event.type != pg.MOUSEMOTION:
				self.botset.clear()
				self.initEmptyPosList()
			elif pg.mouse.get_pressed()[0] and event.type == pg.MOUSEMOTION:
				gW = window.getGridWidth()
				gH = window.getGridHeight()
				exe.spawn(pg.mouse.get_pos(), (gW, gH), False)
		window.drawBots(self.botset, self.emptyset)
		window.updateScreen()
		return False

	def update(self, exe, window):
		self.processInput(exe, window)
		window.incrementGen()
		exe.move()

	def setupDrawGrid(self):
		gridbool = raw_input("Would you like to draw grid? (Y/N): ")
		if gridbool == "y" or gridbool == "Y":
			gridbool = True
		elif gridbool == "n" or gridbool == "N":
			gridbool = False
		else:
			print "Input must be Y or N!"
			sys.exit(0)
		return gridbool

	def getInt(self, message):
		returnVar = raw_input(message)
		try:
			returnVar = int(returnVar)
		except Exception:
			print "Input must be an integer value!"
			sys.exit(0)
		if returnVar < 0:
			print "Input must be equal to or greater than 0!"
			sys.exit(0)
		return returnVar

	def mainloop(self):
		window = GUI(self.SIZE_TUPLE, self.DRAWGRID)
		window.setupScreen()

		exe = Executor(self.botset, self.emptyset, self.SIZE_TUPLE)
		stepbool = True
		firstIteration = True
		while True:
			counter = 0
			if stepbool:
				while counter < self.NUM_CYCLES and not firstIteration:
					self.update(exe, window)
					counter += 1
				if firstIteration:
					window.drawBots(self.botset, self.emptyset)
					window.updateScreen()
			stepbool = self.processInput(exe, window)
			firstIteration = False

def main():
	mainobject = Main()

	mainobject.mainloop()

if __name__ == "__main__":
	main()
