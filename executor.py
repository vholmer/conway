import copy
from robot import *

class Executor:

	def __init__(self, botset, emptyset, SIZE_TUPLE):
		self.botset = botset
		self.emptyset = emptyset
		self.width = SIZE_TUPLE[0]
		self.height = SIZE_TUPLE[1]

		self.NUM_TO_SURVIVE = 2

		self.ITERATIONS = 1

	def move(self):
		botsetCopy = copy.copy(self.botset)
		emptysetCopy = copy.copy(self.emptyset)

		for droid in self.botset:
			if self.getNumAdj(droid) < self.NUM_TO_SURVIVE:
				droid.setAlive(False)
			if self.getNumAdj(droid) == self.NUM_TO_SURVIVE
				pass
			if or self.getNumAdj(droid) == self.NUM_TO_SURVIVE + 1:
				pass
			if self.getNumAdj(droid) > (self.NUM_TO_SURVIVE + 1):
				droid.setAlive(False)

		for emptyPos in emptysetCopy:
			numAdjBots = 0
			adjacent = self.getNeighPos(emptyPos)
			for adjPos in adjacent:
				if adjPos not in emptysetCopy:
					numAdjBots += 1
			if numAdjBots == (self.NUM_TO_SURVIVE + 1):
				self.emptyset.discard(emptyPos)
				self.botset.add(Robot(emptyPos))

		for droid in botsetCopy:
			if droid.isDead():
				pos = droid.getPos()
				self.botset.discard(droid)
				self.emptyset.add(pos)

	def setIterations(self, (x, y)):
		if x == 0 and y == 0:
			self.ITERATIONS = 5
		elif x == 0 and y == self.height:
			self.ITERATIONS = 5
		elif x == self.width and y == 0:
			self.ITERATIONS = 5
		elif x == self.width and y == self.height:
			self.ITERATIONS = 5
		elif x == 0 or x == self.width:
			self.ITERATIONS = 3
		elif y == 0 or y == self.height:
			self.ITERATIONS = 3
		else:
			self.ITERATIONS = 2

	def getNeighPos(self, (x, y)):
		neighborList = []
		neighborList.append((x+1, y))
		neighborList.append((x-1, y))
		neighborList.append((x, y+1))
		neighborList.append((x, y-1))
		neighborList.append((x-1, y-1))
		neighborList.append((x+1, y-1))
		neighborList.append((x-1, y+1))
		neighborList.append((x+1, y+1))

		self.setIterations((x, y))

		for i in range(0,self.ITERATIONS):
			for (x, y) in neighborList:
				if x < 0 or y < 0:
					neighborList.remove((x, y))
				elif x >= self.width or y >= self.height:
					neighborList.remove((x, y))
		return neighborList

# Takes a droid and returns its number of neighbors
	def getNumAdj(self, droid):
		neighborList = droid.getNeighbors()
		self.setIterations(droid.getPos())
		for i in range(0,self.ITERATIONS):
			for (x, y) in neighborList:
				if x < 0 or y < 0:
					neighborList.remove((x, y))
				elif x >= self.width or y >= self.height:
					neighborList.remove((x, y))
		countNeighbors = 0
		for i in neighborList:
			if i not in self.emptyset:
				countNeighbors += 1
		return countNeighbors

	def spawn(self, pos, (gridWidth, gridHeight), deSpawnBool):
		spawnPos = (int(pos[0] / gridWidth), int(pos[1] / gridHeight))
		if spawnPos in self.emptyset:
			self.emptyset.discard(spawnPos)
			self.botset.add(Robot(spawnPos))
		elif spawnPos not in self.emptyset and deSpawnBool:
			self.emptyset.add(spawnPos)
			botsetCopy = copy.copy(self.botset)
			for robot in botsetCopy:
				if robot.getPos() == spawnPos:
					self.botset.discard(robot)
