import pygame as pg
from robot import *

class GUI:

	def __init__(self, dimensions, drawgrid):
		pg.init()

		self.GENERATION = 0
		self.CAPTION = "Generation: "

		self.LINE_WIDTH = 1
		self.SCREEN_WIDTH = 500
		self.SCREEN_HEIGHT = 500
		self.SCREEN_SIZE = (self.SCREEN_WIDTH, self.SCREEN_HEIGHT)

		self.numXgrids = dimensions[0]
		self.numYgrids = dimensions[1]

		self.gridWidth = float(self.SCREEN_WIDTH) / float(self.numXgrids)
		self.gridHeight = float(self.SCREEN_HEIGHT) / float(self.numYgrids)

		self.white = (255, 255, 255)
		self.black = (0, 0, 0)
		self.red = (255, 0, 0)
		self.green = (0, 255, 0)
		self.blue = (0, 0, 255)

		self.BOTCLR = self.red
		self.LINECLR = self.black
		self.EMPTYCLR = self.white

		self.gridbool = drawgrid

	def setupScreen(self):
		self.screen = pg.display.set_mode(self.SCREEN_SIZE)
		self.screen.fill(self.white)

	def drawGrid(self):
		x = 0
		y = 0
		while x < self.SCREEN_WIDTH:
			y = 0
			pg.draw.line(self.screen, self.LINECLR, (x, y), (x, self.SCREEN_HEIGHT), self.LINE_WIDTH)
			while y < self.SCREEN_HEIGHT:
				pg.draw.line(self.screen, self.LINECLR, (x, y), (self.SCREEN_WIDTH, y), self.LINE_WIDTH)
				y += self.gridHeight
			x += self.gridWidth
		pg.display.update()

	def drawBots(self, botset, emptyset):
		for emPos in emptyset:
			posX = emPos[0] * self.gridWidth if emPos[0] != 0 else 0
			posY = emPos[1] * self.gridHeight if emPos[1] != 0 else 0
			drawPos = (posX, posY)
			pg.draw.rect(self.screen, self.EMPTYCLR, pg.Rect(posX, posY, self.gridWidth + self.LINE_WIDTH, self.gridHeight + self.LINE_WIDTH), 0)
		for droid in botset:
			robPos = droid.getPos()
			posX = robPos[0] * self.gridWidth if robPos[0] != 0 else 0
			posY = robPos[1] * self.gridHeight if robPos[1] != 0 else 0
			drawPos = (posX, posY)
			pg.draw.rect(self.screen, self.BOTCLR, pg.Rect(posX, posY, self.gridWidth + self.LINE_WIDTH, self.gridHeight + self.LINE_WIDTH), 0)
		if self.gridbool:
			self.drawGrid()

	def updateScreen(self):
		pg.display.set_caption(self.CAPTION + str(self.GENERATION))
		pg.display.flip()

	def incrementGen(self):
		self.GENERATION += 1

	def getGridWidth(self):
		return self.gridWidth

	def getGridHeight(self):
		return self.gridHeight
