## Conway
Created in python 2.7.9 with pygame.

This is a simple Conway's game of life simulation.
Created using the pygame library.
Press escape to quit the simulation.
Press space to advance.
Press C to clear the grid.

## TODO
* Make program independent of pygame / make go, java or some other version.
* Implement 5*5 square algorithm for checking if procreation should happen.
* Make drawing concurrent!
* Don't keep bots in an unordered set, you fool. This is extremely inefficient.