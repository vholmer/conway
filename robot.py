class Robot:

	def __init__(self, position):
		self.position = position
		self.alive = True

	def getPos(self):
		return self.position

	def setAlive(self, aliveBool):
		if not aliveBool:
			self.alive = False
		else:
			self.alive = True

	def isDead(self):
		if self.alive:
			return False
		else:
			return True

	def getNeighbors(self):
		returnPos = []
		leftNeighbor = (self.position[0] - 1, self.position[1])
		rightNeighbor = (self.position[0] + 1, self.position[1])
		upNeighbor = (self.position[0], self.position[1] - 1)
		downNeighbor = (self.position[0], self.position[1] + 1)

		topLeftNeighbor = (self.position[0] - 1, self.position[1] - 1)
		topRightNeighbor = (self.position[0] + 1, self.position[1] - 1)
		botLeftNeighbor = (self.position[0] - 1, self.position[1] + 1)
		botRightNeighbor = (self.position[0] + 1, self.position[1] + 1)

		returnPos.append(leftNeighbor)
		returnPos.append(rightNeighbor)
		returnPos.append(upNeighbor)
		returnPos.append(downNeighbor)

		returnPos.append(topLeftNeighbor)
		returnPos.append(topRightNeighbor)
		returnPos.append(botLeftNeighbor)
		returnPos.append(botRightNeighbor)

		return returnPos
